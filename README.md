# JavaStudyNotes

#### 介绍
Java学习笔记

#### 基础篇
[Spring知识总结](https://gitee.com/lordum/java-study-notes/blob/master/Spring%E7%9F%A5%E8%AF%86%E6%80%BB%E7%BB%93.md)
#### 架构篇
[高并发系统40问](https://gitee.com/lordum/java-study-notes/blob/master/%E9%AB%98%E5%B9%B6%E5%8F%91%E7%B3%BB%E7%BB%9F40%E9%97%AE.md)
#### 面试篇
[Java核心技术面试精讲笔记](https://gitee.com/lordum/java-study-notes/blob/master/Java%E6%A0%B8%E5%BF%83%E6%8A%80%E6%9C%AF%E9%9D%A2%E8%AF%95%E7%B2%BE%E8%AE%B2%E7%AC%94%E8%AE%B0.md)